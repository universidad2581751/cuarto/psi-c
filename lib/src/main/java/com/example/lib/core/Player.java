package com.example.lib.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Player {

    private int id, points, puddingCounter, makiCount;
    private ArrayList<Card> currentHand; // hand where you must choose a card
    private ArrayList<Card> tableHand;   // cards already on the table


    public Player (int id) {
        
        this.id = id;
        this.points = 0;
        this.puddingCounter = 0;
        this.makiCount = 0;
    }


    public int getMakiCount () {

        return makiCount;
    }


    public int getId () {
        
        return this.id;
    }


    public void setPoints (int points) {
        
        this.points = points;
    }


    public int getPoints () {
        
        return this.points;
    }


    public ArrayList<Card> getCurrentHand () {

        return this.currentHand;
    }


    public void setCurrentHand (ArrayList <Card> currentHand) {
        this.currentHand = currentHand;
    }


    public ArrayList<Card> getTableHand () {
        return this.tableHand;
    }


    public void setCurrentTable(ArrayList <Card> tableHand) {
        this.tableHand = tableHand; //debugging, probably para puntuación
    }


    public void addPoints(int points) {
        this.points += points;
    } 


    public void playCard (String index) {

        for (Card card : currentHand) {
            if (card.getType().equals(index)) {
                tableHand.add (card);
                currentHand.remove (card);
                return;
            }
        }
        System.out.println ("No sabes leer las cartas que tienes en la mano, retrasado");
    }


     public String toString() {
        String res = "Player " + this.getId() + " has the cards: \n";
        for (Card card : this.currentHand) {
            res += card.getType() + ", ";
        }
        return res;
    }


    public int countPoints () {

        HashMap <String, Integer> typeCountMap = new HashMap<>();
        int pointsGained = 0;

        for (Card card : tableHand) { // Si quieres probarlo sin jugar nada cambia esto a currentHand

            String type = card.getType();
            typeCountMap.put(type, typeCountMap.getOrDefault(type, 0) + 1);
        }

        for (Map.Entry <String, Integer> entrada : typeCountMap.entrySet ()) {

            switch (entrada.getKey ()) {

                case "MakiRoll":

                    makiCount = 2 * entrada.getValue ();
                    System.out.println ("Makis conseguidos = " + (2 * entrada.getValue ()));
                    break;

                case "Sashimi":

                    pointsGained += (entrada.getValue () - (entrada.getValue () % 3)) / 3 * 10;
                    System.out.println ("Puntos por sashimi = " + (entrada.getValue () - (entrada.getValue () % 3)) / 3 * 10);
                    break;                
                
                case "Tempura":
                
                    pointsGained += ((entrada.getValue () - (entrada.getValue () % 2)) / 2) * 5;
                    System.out.println ("Puntos por tempura = " + (entrada.getValue () - (entrada.getValue () % 2)) / 2 * 5);
                    break;
                
                case "SalmonNiguiri":
                
                    pointsGained += 2 * entrada.getValue ();
                    System.out.println ("Puntos por niguiri = " + (entrada.getValue() * 2));
                    break;
                
                case "Dumpling":
                
                    pointsGained += (Math.min (5, entrada.getValue ()) * (Math.min (5, entrada.getValue ()) + 1)) / 2;
                    System.out.println ("Puntos por dumplings = " + (Math.min (5, entrada.getValue ()) * (Math.min (5, entrada.getValue ()) + 1)) / 2);
                    break;
                
                case "Pudding":

                    puddingCounter += entrada.getValue ();
                    System.out.println ("Pudding conseguido = " + entrada.getValue ());
                    break;
            }

            this.points += pointsGained;
        }

        return pointsGained;
    }
}
