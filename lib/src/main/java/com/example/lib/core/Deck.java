package com.example.lib.core;

import java.util.ArrayList;
import java.util.Collections;

public class Deck {

    private int nMakiRollCards = 26;
    private int nShashimiCards = 14;
    private int nTempuraCards = 14;
    private int nSalmonNiguiriCards = 20;
    private int nDumplingCards = 14;
    private int nPuddingCards = 10;
    private ArrayList<Card> cards;

    public Deck() {
        this.cards = new ArrayList<>();
        createDeckCards();
    }

    public ArrayList<Card> getAHand(int nCards) {

        ArrayList<Card> hand = new ArrayList<>();

        if (nCards > 0 && nCards <= cards.size()) {
            Collections.shuffle(cards);
            for (int i = 0; i < nCards; i++) {
                hand.add(cards.remove(0));
            }
        }
        return hand;
    }


    // create the cards for the deck
    private void createDeckCards() {
        // Create Maki Roll cards
        for (int i = 0; i < nMakiRollCards; i++) {
            cards.add(new Card("MakiRoll"));
        }

        // Create Shashimi cards
        for (int i = 0; i < nShashimiCards; i++) {
            cards.add(new Card("Shashimi"));
        }

        // Create Tempura cards
        for (int i = 0; i < nTempuraCards; i++) {
            cards.add(new Card("Tempura"));
        }

        // Cerate Salmon Niguiri cards
        for (int i = 0; i < nSalmonNiguiriCards; i++) {
            cards.add(new Card("SalmonNiguiri"));
        }

        // Create Dumpling cards
        for (int i = 0; i < nDumplingCards; i++) {
            cards.add(new Card("Dumpling"));
        }

        // Cerate Pudding cards
        for (int i = 0; i < nPuddingCards; i++) {
            cards.add(new Card("Pudding"));
        }
    }
}
