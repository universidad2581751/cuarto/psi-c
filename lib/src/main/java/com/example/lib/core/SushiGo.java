package com.example.lib.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;

public class SushiGo {

    private Deck deck;
    ArrayList <Player> players = new ArrayList <Player> ();
    private int initialHandSize = 8;
    private int playerNumber = 4;

    public SushiGo() {
        this.deck = new Deck ();
        initPlayers ();
        initHands ();
        initTable();
        play ();
    }
    
    private void play() {

        for (int gameNum = 0; gameNum < 3; gameNum++) {
            dealCards();
            
            for (int roundNum = 0; roundNum < initialHandSize; roundNum++) {
                playerPlay(players.get(1));
                playerPlay(players.get(2));
                playerPlay(players.get(3));

                humanPlayerPlay(players.get(0));

                for (Player p : players) {
                    System.out.println(p + " despues de jugar\n\n");
                }
                if (!players.get(0).getCurrentHand().isEmpty()){
                    
                    rotateHands();
                    
                    for (Player p : players) {
                        System.out.println(p + " despues de rotar\n\n");
                    }
                }                
                /*
                 * Aqui ven o tema de que os xogadores IA escollan carta
                 * e queda a partida en "wait" ata que o xogador persoa
                 * escolle carta
                 */
                countPoints();
                //  ver de asignar e gardar puntos. De momento, solo dun xogo, 3 rondas.
                //  asignar_puntuacion();
            }
        }
    }

    private void initPlayers() {
        for (int i = 0; i < playerNumber; i++) {
            players.add(new Player (i));
        }
    }

    private void initHands() {
        for (Player player : players) {
            ArrayList<Card> initialHand = new ArrayList<Card>();
            player.setCurrentHand(initialHand);
        }
    }

    private void initTable() {
        for (Player player : players) {
            ArrayList<Card> initialTable = new ArrayList<Card>();
            player.setCurrentTable(initialTable);
        }
    }

        private void dealCards() {
        for (Player player : players) {
            ArrayList<Card> dealCards = deck.getAHand(initialHandSize);
            player.setCurrentHand(dealCards);
            System.out.println(Collections.frequency(player.getCurrentHand(), new Card("MakiRoll")));
        }
    }

    private void playerPlay(Player player){ //probar primeiro RANDOM
        Random r1 = new Random();
        ArrayList<Card> hand = player.getCurrentHand();
        int rCard = r1.nextInt(hand.size());
        Card selCard = hand.get(rCard);
        player.playCard(selCard.getType());
 //     System.out.println(player.getTableHand()+ "\n") ; print para ver se gardaba ben a mesa   
    }

    private void humanPlayerPlay(Player player){
        Scanner sc = new Scanner(System.in);
        System.out.println("Tus cartas: " + player);
        String myCard = sc.next();
        player.playCard(myCard);
    }

    private void rotateHands() {
        ArrayList<Card> auxHand = new ArrayList<Card>();
        auxHand.addAll(players.get(0).getCurrentHand());
        
        for (int i = 1; i < playerNumber; i++) {
            players.get(i-1).setCurrentHand(players.get(i).getCurrentHand()); //do xogador 1 ao 0, do 2 ao 1 etc
        }
        players.get(playerNumber-1).setCurrentHand(auxHand);
    } 


    public void countPoints () {

        int bestCount = 0, secondBest = 0;

        for (Player player : players) {

            player.countPoints ();

            if (player.getMakiCount () >= bestCount) {

                bestCount = player.getMakiCount ();
            
            } else if (player.getMakiCount () > secondBest) {

                secondBest = player.getMakiCount ();
            }
        }

        for (Player player : players) {

            if (player.getMakiCount () == bestCount) {

                player.addPoints (6);

            } else if (player.getMakiCount () == secondBest) {

                player.addPoints (3);
            }
        }
    }
}
