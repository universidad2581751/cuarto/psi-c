package core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;
import java.util.Scanner;

public class SushiGo {

    private Deck deck;
    ArrayList <Player> players = new ArrayList <Player> ();
    private int initialHandSize = 8;
    private int playerNumber = 4;

    Minmax minmax;

    public SushiGo() {
        this.deck = new Deck ();
        initPlayers ();
        initHands ();
        initTable();
        dealCards();//por ahora
        minmax = new Minmax();
        //play ();
    }

    private void play() {

        
        for (int gameNum = 0; gameNum < 1; gameNum++) {
            dealCards();

            for (int roundNum = 0; roundNum < initialHandSize; roundNum++) {

                if (roundNum == 0) {
                    // || roundNum == (initialHandSize-1)
                    firstPlay(players.get(1));
                    firstPlay(players.get(2));
                    firstPlay(players.get(3));
                }
                else {
                    iaPlayerPlay(players.get(1),  players.get(2));
                    iaPlayerPlay(players.get(2),  players.get(3));
                    iaPlayerPlay(players.get(3),  players.get(0));
                }
                //humanPlayerPlay(players.get(0));

                rotateHands();
            }

            /* for (int i = 0; i < players.size (); i++) {

                System.out.println ("Player " + players.get (i).getId ()  + " has played: " + players.get (i).getTableHand ());
            } */

            countPoints ();

            /* for (int i = 0; i < players.size (); i++) {

                System.out.println ("Player " + players.get (i).getId()  + " have " + players.get (i).getPoints () + " points");
            } */
        }

        System.out.println("\n\nFINAL");
        countPuddings();

        for (int i = 0; i < players.size (); i++) {
            System.out.println ("Player " + players.get (i).getId ()  + " have " + players.get (i).getPoints () + " points");
        }

    }

    private void initPlayers() {
        for (int i = 0; i < playerNumber; i++) {
            players.add(new Player (i));
        }
    }

    private void initHands() {
        for (Player player : players) {
            ArrayList<Card> initialHand = new ArrayList<Card>();
            player.setCurrentHand(initialHand);
        }
    }

    private void initTable() {
        for (Player player : players) {
            ArrayList<Card> initialTable = new ArrayList<Card>();
            player.setCurrentTable(initialTable);
        }
    }

    public void dealCards() {
        for (Player player : players) {
            ArrayList<Card> dealCards = deck.getAHand(initialHandSize);
            player.resetTable ();
            player.setCurrentHand(dealCards);
        }
    }

    public void iaPlayerPlay(Player iaPlayer, Player nextPlayer){
        //System.out.println("\nTurno de la IA " + iaPlayer.getId());        
        //System.out.println("Cartas en la mesa: " + iaPlayer.getTableHand());
        //System.out.println("Cartas en la mano: " + iaPlayer.getCurrentHand());
        

        String selCard = minmax.selectCard(iaPlayer, nextPlayer);
        iaPlayer.playCard(selCard);
        
        //System.out.println("IA " + iaPlayer.getId() + " escolle:" + selCard);        
        //System.out.println("Cartas en la mesa despues de coger: " + iaPlayer.getTableHand());
        //System.out.println("Cartas en la mano despues de coger: " + iaPlayer.getCurrentHand());

    }

    public void humanPlayerPlay(Player player, String selectedCard){
        //System.out.println("Tus cartas: " + player.getCurrentHand ());
        //System.out.println("Carta jugada: " + selectedCard);
        player.playCard(selectedCard);
    }

    /* public void randomPlayerPlay(Player player){ //probar primeiro RANDOM
        Random r1 = new Random();
        ArrayList<Card> hand = player.getCurrentHand();
        int randomCard = r1.nextInt(hand.size());
        Card selectedCard = hand.get(randomCard);
        player.playCard(selectedCard.getType());
        //     System.out.println(player.getTableHand()+ "\n") ; print para ver se gardaba ben a mesa
    } */

    public void firstPlay(Player player){
        ArrayList<Card> hand = player.getCurrentHand();
        ArrayList<Card> randomBest = new ArrayList<Card>();
        Card selectedCard;

        for (Card card : hand) {
            if (card.getType().equals("Dumpling") || card.getType().equals("MakiRoll") || card.getType().equals("Sashimi")) {
                randomBest.add(card);
            }
        }

        Random random = new Random();
        if(!randomBest.isEmpty()){
            selectedCard = randomBest.get(random.nextInt(randomBest.size()));
            player.playCard(selectedCard.getType());
            return;
        }


        // If it comes out A we give another opportunity to choose another randomly
        random = new Random();
        selectedCard = hand.get(random.nextInt(hand.size()));

        if (selectedCard.getType().equals("SalmonNiguiri")) {
            random = new Random();
            selectedCard = hand.get(random.nextInt(hand.size()));
        }
        player.playCard(selectedCard.getType());
    }

    public void rotateHands() {
        ArrayList<Card> auxHand = new ArrayList<Card>();
        auxHand.addAll(players.get(0).getCurrentHand());

        for (int i = 1; i < playerNumber; i++) {
            players.get(i-1).setCurrentHand(players.get(i).getCurrentHand()); //do xogador 1 ao 0, do 2 ao 1 etc
        }
        players.get(playerNumber-1).setCurrentHand(auxHand);
    }


    public void countPoints () {

        int bestCount = 0, secondBest = 0;
        int first = 0, second = 0;

        for (Player p : players) {
            System.out.println ("P" + p.getId () + " - Point gained: " + p.countPoints () + "\n");
        }

        Collections.sort(players, Comparator.comparingInt(Player::getMakiCount));
        bestCount = players.get(players.size()-1).getMakiCount();

        for (Player p : players) {
            if (p.getMakiCount() == bestCount) {
                first++;
            }
        }

        if (first == 1) {
            secondBest = players.get(players.size() - 2).getMakiCount();
            for (Player p : players) {
                if (p.getMakiCount() == secondBest) {
                    second++;
                }
            }

            int pointsSec = (3-(3 % second))/second;

            for (int i = players.size()-2; i > players.size() - 2 - second; i--) {
                players.get(i).addPoints(pointsSec);
            }

        }

        int pointsFirst = (6-(6 % first))/first;

        for (int i = players.size()-1; i > players.size() - 1 - first; i--) {
            players.get(i).addPoints(pointsFirst);
        }

        Collections.sort(players, Comparator.comparingInt(Player::getId));
    }

    public void countPuddings() {
        int worst = 0;
        int best = 0;

        Collections.sort(players, Comparator.comparingInt(Player::getPuddings));

        for (Player p : players) {
            if (p.getPuddings() == players.get(0).getPuddings()) {
                worst++;
            } else if (p.getPuddings() == players.get(players.size()-1).getPuddings()) {
                best++;
            }
        }

        int points = (6-(6 % worst))/worst;
        System.out.println ("Point for the worst: " + points);

        for (int i=0; i < worst; i++) {
            players.get(i).addPoints(-points);
        }

        points = (6-(6 % best))/best;
        System.out.println ("Point for the best: " + points);

        for (int i = players.size()-1; i > players.size()-1-best; i--) {
            players.get(i).addPoints(points);
        }

        Collections.sort(players, Comparator.comparingInt(Player::getId));

    }

    public Player getHumanPlayer(){
        return players.get(0);
    }
    public ArrayList <Player> getPlayers(){
        return players;
    }

    public int getInitialHandSize(){
        return initialHandSize;
    }

}
