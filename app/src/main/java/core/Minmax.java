//Código de referencia: https://www.geeksforgeeks.org/minimax-algorithm-in-game-theory-set-1-introduction/
//Para alpha beta (unha evolucion de minimax): https://www.geeksforgeeks.org/minimax-algorithm-in-game-theory-set-4-alpha-beta-pruning/
//hay comentarios polo medio, pero resumen:
// - a lógica do prototipo é moi simple, por probas que fixen aseméjase á eficiencia do random; sigue unhas reglas arbitrarias impostas por min
// - non ten en conta cartas xa xogadas por ningún xogador, nin cartas na man do resto, que xa pasaran polas mans da ia en cuestion e sabería cales son
// - non ten en conta cartas repetidas na man, é dicir, non ten en conta para a eleccion que na man teña varios dun mismo tipo, que poderían cambiar a decision (cambiando o valor da "eleccion", xa sexa reducindo ou aumentando)

package core;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;


public class Minmax {

    private static int MAX_DEPTH = 2; // Max value to deph  
    private String bestCard = null;
    private String iaLocalBestCard = null;
    private String nextLocalBestCard = null;
    
    public Minmax() {}

    public String selectCard(Player iaPlayer, Player nextPlayer) {

        // Inicializo
        String[] iaHand = (iaPlayer.getCurrentHandToString()).split(", ");
        String[] iaTable = (iaPlayer.getTableHandToString()).split(", ");
        String[] nextPlayerTable = (nextPlayer.getTableHandToString()).split(", ");

        double bestResult = maxValue(iaHand, iaTable, null, nextPlayerTable, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, 0);
 
        return bestCard;
    }

    private double maxValue(String[] iaHand, String[] iaTable, String[] nextPlayerHand, String[] nextPlayerTable, double alpha, double beta, int depth) {

		if (depth >= MAX_DEPTH) {
			return evaluation(iaTable, nextPlayerTable);
		}
		
		//String iaLocalBestCard = null;
        //String[] localIaTable = iaTable;
        String[] localFutureIaHand = null; // non imos ter en conta o q nos pode vir do xogador anterior
        //String[] localFutureNextPlayerHand = iaHand.clone(); // nesta variable iremos estudando a mellor opcion
        String[] localNextPlayerTable = nextPlayerTable;
		double bestResult = Double.NEGATIVE_INFINITY;

        List<String> localIaTable = new ArrayList<>(Arrays.asList(iaTable));
        List<String> localFutureNextPlayerHand = new ArrayList<>(Arrays.asList(iaHand.clone()));
		
		/*
         * En cada iteracion supoñemos que o xogador escolle a carta i e analizamos que puntos pode conseguir
         * o seguinte xogador coas cartas que ten na mesa e a man que terá no seguinte turno
		*/

		for (int i = 0; i < iaHand.length; i++) {
            
            iaLocalBestCard = iaHand[i];
            localFutureNextPlayerHand.remove(iaLocalBestCard);
            localIaTable.add(iaLocalBestCard);

            /* Recursion 
             * Non imos ter en conta o que pode escoller no turno actual o seguinte xogador, só o que 
             * podería obter se lle damos a man de cartas coas que xa ten na mesa
            */
            Double evaluation = minValue(localFutureIaHand, localIaTable.toArray(new String[0]), localFutureNextPlayerHand.toArray(new String[0]), localNextPlayerTable, alpha, beta, depth+1);

            bestResult = Math.max(bestResult, evaluation);
            alpha = Math.max(alpha,bestResult);
            //alpha = Math.max (alpha, minValue(localFutureIaHand, localIaTable.toArray(new String[0]), localFutureNextPlayerHand.toArray(new String[0]), localNextPlayerTable, alpha, beta, depth+1));
            //System.out.println("\nNodo MAX:");
            //System.out.println("ALPHA = " + alpha);
            //System.out.println("BETA = " + beta);
            /* Is this move better than another found before? */
            /* if (alpha > bestResult) {
                iaLocalBestCard = iaHand[i];
                bestResult = alpha;
            } */
        
            /* Now we test if we can do a break */
            if (alpha >= beta) {
                //System.out.println("(II) Beta cut!");
                //System.out.println("CORTO RAMA EN MAX");
                break;
            }
            
            /* Resetting local hands and tables */
            localFutureNextPlayerHand.add(iaLocalBestCard);
            localIaTable.remove(iaLocalBestCard);

		} /* End of looping */
		
		bestCard = iaLocalBestCard;
		return alpha;
		
	}

    private double minValue(String[] iaHand, String[] iaTable, String[] nextPlayerHand, String[] nextPlayerTable, double alpha, double beta, int depth) {
		
		if (depth >= MAX_DEPTH) {
			return evaluation(iaTable, nextPlayerTable);
		}
		
		//String nextLocalBestCard = null;
        //String[] localIaTable = iaTable;
        String[] localIaHand = iaHand;
        String[] localFutureNextPlayerHand = nextPlayerHand;
        //String[] localNextPlayerTable = nextPlayerTable;
		double bestResult = Double.POSITIVE_INFINITY;

        List<String> localIaTable = new ArrayList<>(Arrays.asList(iaTable));
        List<String>  localNextPlayerTable =  new ArrayList<>(Arrays.asList(nextPlayerTable));
		
		for (int i = 0; i < localFutureNextPlayerHand.length; i++) {
            
            //System.out.println("\nVALIANDO EN MIN, A CARTA A AVALIAR É " + localFutureNextPlayerHand[i]);
            nextLocalBestCard = localFutureNextPlayerHand[i];
            //localFutureNextPlayerHand.remove(nextLocalBestCard);
            localNextPlayerTable.add(nextLocalBestCard);

            /* Recursion 
             * Non imos ter en conta o que pode escoller no turno actual o seguinte xogador, só o que 
             * podería obter se lle damos a man de cartas coas que xa ten na mesa
            */
            //beta = Math.min(beta, maxValue(null, localIaTable.toArray(new String[0]), null, localNextPlayerTable.toArray(new String[0]), alpha, beta, depth+1));
            Double evaluation = maxValue(null, localIaTable.toArray(new String[0]), null, localNextPlayerTable.toArray(new String[0]), alpha, beta, depth+1);
            
            bestResult = Math.min(bestResult, evaluation);
            beta = Math.min(beta,bestResult);
            
            //System.out.println("\nNodo MIN:");
            //System.out.println("ALPHA = " + alpha);
            //System.out.println("BETA = " + beta);
            
            /* Is this move better than another found before? */
            /* if (beta < bestResult) {
                bestResult = beta;
            } */

            if (beta <= alpha) {
                //System.out.println("CORTO RAMA EN MIN");
                break;
            }

		} /* End of looping */
		
		return beta;
		
	}
	
    private double evaluation(String[] iaTable, String[] nextPlayerTable) {
        
        //System.out.println("\nCARTA A VALORAR IA : " + iaLocalBestCard);
        //System.out.println("\nCARTA A VALORAR NEXT : " + nextLocalBestCard);

        int iaPoints = 0;
        int nextPlayerPoints = 0;

        // Calculating IA points
        ArrayList<Card> auxCards = new ArrayList<Card>();

        for (String card : iaTable) {
            auxCards.add(new Card(card)); 
        }

        Player auxPlayer = new Player(-1);
        auxPlayer.setCurrentTable(auxCards);
        iaPoints += auxPlayer.countPoints();
        ////System.out.println("PUNTOS IA ANTES DE EXTRAS = " + iaPoints);
        // Calculating nextPlayer points
        auxCards = new ArrayList<Card>();

        for (String card : nextPlayerTable) {
            auxCards.add(new Card(card)); 
        }

        auxPlayer = new Player(-1);
        auxPlayer.setCurrentTable(auxCards);
        nextPlayerPoints += auxPlayer.countPoints();
        //System.out.println("PUNTOS NEXT ANTES DE EXTRAS = " + nextPlayerPoints);

        // We add extra points to give more weight to taking certain cards
        iaPoints += assignedPoints(iaTable, iaLocalBestCard);
        nextPlayerPoints += assignedPoints(nextPlayerTable, nextLocalBestCard);

        // result

        //System.out.println("PUNTOS IA CON EXTRAS = " + iaPoints);
        //System.out.println("PUNTOS NEXT CON EXTRAS = " + nextPlayerPoints);
        //System.out.println("DIFERENCIA = " + (iaPoints - nextPlayerPoints));

        //return iaPoints - nextPlayerPoints;
        return  nextPlayerPoints - iaPoints ;
    }

    
    private int assignedPoints(String[] cards, String localBestCard) {

        int points = 0;
        int makiRollNum = 0;
        int tempuraNum = 0;
        int sashimiNum = 0;
        int dumplingNum = 0;
        int niguiriNum = 0;
        int puddingNum = 0;
        int totalCards = 0;

        for (String card : cards) {
            switch(card) {
                case "MakiRoll":
                    makiRollNum++;
                    break;
                case "Tempura":
                    tempuraNum++;
                    break;
                case "Sashimi":
                    sashimiNum++;
                    break;
                case "Dumpling":
                    dumplingNum++;
                    break;
                case "SalmonNiguiri":
                    niguiriNum++;
                    break;
                case "Pudding":
                    puddingNum++;
                    break;
                default:
                    break;  
            }
            totalCards++;
        }

        // arrisca máis nos primeiros turnos
        if (totalCards < 5) {
            if (sashimiNum < 3 && localBestCard.equals("Sashimi"))
                points += 5;

            if (sashimiNum == 2 && localBestCard.equals("Sashimi"))
                points += 10;

            if (tempuraNum < 1 && localBestCard.equals("Tempura"))
                points += 4;

            if (tempuraNum == 1 && localBestCard.equals("Tempura"))
                points += 8;

            if (tempuraNum == 3 && localBestCard.equals("Tempura"))
                points += 8;

            if (dumplingNum < 3 && localBestCard.equals("Dumpling"))
                points += 6;

            if (dumplingNum >= 3 && localBestCard.equals("Dumpling"))
                points += 12;

            if (makiRollNum <= 3 && localBestCard.equals("MakiRoll"))
                points += 1;

            if (makiRollNum > 3 && localBestCard.equals("MakiRoll"))
                points -= 6;

            if (puddingNum < 3 && localBestCard.equals("Pudding"))
                points += 2;

            if (niguiriNum < 5 && localBestCard.equals("SalmonNiguiri"))
                points -= 1;

        }
        // mais resolutivo
        else {
            if (sashimiNum >= 3 && localBestCard.equals("Sashimi"))
                points -= 5;

            if (sashimiNum == 2 && localBestCard.equals("Sashimi"))
                points += 10;

            if (sashimiNum == 1 && localBestCard.equals("Sashimi"))
                points += 6;

            if (makiRollNum <= 3 && localBestCard.equals("MakiRoll"))
                points += 1;

            if (makiRollNum > 3 && localBestCard.equals("MakiRoll"))
                points -= 6;

            if (dumplingNum >= 3 && localBestCard.equals("Dumpling"))
                points += 10;

            if (dumplingNum < 3 && localBestCard.equals("Dumpling")) 
                points -= 1;

            if (tempuraNum == 1 && localBestCard.equals("Tempura"))
                points += 4;

            if (tempuraNum >=4 && localBestCard.equals("Tempura"))
                points -= 2;

            if (tempuraNum == 3 && localBestCard.equals("Tempura"))
                points += 4;

            if (puddingNum <= 2 && localBestCard.equals("Pudding"))
                points += 3;

            if (puddingNum > 2 && localBestCard.equals("Pudding"))
                points -= 1;

            if (niguiriNum < 8 && localBestCard.equals("SalmonNiguiri"))
                points += 2;
        }

        return points;
    }

}