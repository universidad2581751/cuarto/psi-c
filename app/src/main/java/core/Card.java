package core;

public class Card {

    private String type;
    
    public Card() {}

    public Card(String type) {
        this.type = type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return this.type;
    }

    public String toString () {

        return type;
    }
}
