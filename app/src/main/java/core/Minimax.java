//Código de referencia: https://www.geeksforgeeks.org/minimax-algorithm-in-game-theory-set-1-introduction/
//Para alpha beta (unha evolucion de minimax): https://www.geeksforgeeks.org/minimax-algorithm-in-game-theory-set-4-alpha-beta-pruning/
//hay comentarios polo medio, pero resumen:
// - a lógica do prototipo é moi simple, por probas que fixen aseméjase á eficiencia do random; sigue unhas reglas arbitrarias impostas por min
// - non ten en conta cartas xa xogadas por ningún xogador, nin cartas na man do resto, que xa pasaran polas mans da ia en cuestion e sabería cales son
// - non ten en conta cartas repetidas na man, é dicir, non ten en conta para a eleccion que na man teña varios dun mismo tipo, que poderían cambiar a decision (cambiando o valor da "eleccion", xa sexa reducindo ou aumentando)
/*
package core;
import java.util.ArrayList;

public class Minimax {
    private static ArrayList<Card> hand = new ArrayList<>();

public static int minimax(int[] puntuacionCartas, int index, int depth, int jugador, int nMax) {
        if (depth == nMax  ) {
            return puntuacionCartas[index];
        }
        if (jugador > 0) { //o id 0 é o do humano, o único que non vai ser nunca. lógica para que vaia de maximizar a minimizar,
            //podese introducir outro valor. a aclaracion solo sirve para a primeira vez
           return Math.max( minimax(puntuacionCartas, index*2, depth +1, 0, nMax), 
                            minimax(puntuacionCartas, index*2+1, depth +1, 0, nMax));
        } else {
            return Math.min( minimax(puntuacionCartas, index*2, depth +1, 1, nMax), 
                             minimax(puntuacionCartas, index*2+1, depth +1, 1, nMax));
        }
    } 


    static int log2(int n){
        return (n==1)? 0 : 1 + log2(n/2);
    }


    public static void playerPlay( Player player){ //ArrayList <Player> players,
        hand = player.getCurrentHand();
        System.out.println(player.getCurrentHand().toString());
        Card cartaAComparar = new Card();
        int[] scores = new int[hand.size()];

        //cambiar lógica a posteriori, vou pór valores arbitrarios, empezando cunha logica simple para ver como se comporta
        //pudding mayor valor, dumpling menor valor. vou facer unha escala ascendente e punto.
        //o funcionamento pode ser simplemente que prediga a seguinte xogada do xogador ao que lle se da as cartas no seguinte turno
        //-así vale con ver as cartas que ten na man solo
        //-non ten en conta as cartas xa xogadas
        
        for(int i = 0; i<hand.size(); i++){
            int valorCarta=0;
            cartaAComparar = player.getCurrentHand().get(i);
            //cartaHumana = players.get(0).getCurrentHand().get(i); 
             switch (cartaAComparar.getType()) {
                case "MakiRoll":
                    valorCarta = 1;
                    break;
                case "Sashimi":
                    valorCarta = 5;
                    break;
                case "Tempura":
                    valorCarta = 4;
                    break;
                case "SalmonNiguiri":
                    valorCarta = 2;
                    break;
                case "Dumpling":
                    valorCarta = 6;
                    break;
                case "Pudding":
                    valorCarta = 3;
                    break;
            }
            scores[i] = valorCarta;
        }
        //
        //igual poden facerse máis iteracions, non se me ocorre cómo de momento     
        //int jugador = players.get(0).getId(); id do humano, creo que non fai falta ao final
        //
        int n;
        n = log2(scores.length);
        
        int mejorCarta = minimax(scores,0, 0, player.getId(), n);//aquí chama a onde surge la magia. valores q mandarlle, inda placeholder    
        System.out.println("El valor de la mejor carta es: " + mejorCarta);

        //esta forma toca o menos posible o codigo do minimax, non é a óptima pero funciona. 
        //Revirto a puntuacion que me inventei e volvo pór a carta que quero usar nun auxiliar:
        Card selCard = new Card();
        switch (mejorCarta) {
            case 6:
                selCard.setType("Dumpling");
                break;
            case 5:
                selCard.setType("Sashimi");
                break;
            case 4:
                selCard.setType("Tempura");
                break;
            case 2:
                selCard.setType("SalmonNiguiri");
                break;
            case 1:
                selCard.setType("MakiRoll");
                break;
            case 3:
                selCard.setType("Pudding");
                break;
            }
        System.out.println("Carta elegida por Player"+player.getId() +": " + selCard.getType() + "\n");
        player.playCard(selCard.getType());
    }

}




/*
 * Normalmente este algoritmo intercala entre profuncidad Max (tu jugada) y min (la jugada del contrario)
 * Al ser 4 jugadores y la profuncidad maxima 3 solo pasa por el max la primera vez y despues es recursivo
 * en las capas min
 */
/*
public class ABPruning {

    private static int MAX_DEPTH = 3;
    private Card bestPlay = null;


    /* 
     * Aquí hay que buscar formas de que la ia sepa que jugador es, que cartas tiene en ese momento (y en el mejor de los casos que cartas
     * tienen jugadas los demas jugadores)
     */

     /*
      * Viendo varias cosas lo mejor seria hacer un array con las cartas jugadas de todos los jugadores (en orden de jugador)
      * y otro con las cartas de la mano que tiene actualmente
      */

    /*
     * @param currentHand la mano con la que juega
     * @param currentTable tiene en cada posicion del array un arraylist con las cartas en la mesa del jugador correspondiente
     * @param playerId la posicion del array currentTable en la que esta su mesa
     */
/*
    public Card nextMove (ArrayList <Card> currentHand, ArrayList <Card> [] currentTable, int playerId) {

        double jugada = -1;

        if (currentHand.size () == 1) {


        }

        maxNode (currentTable, currentHand, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, Math.min (0, 3 - currentHand.size ()) + 1, playerId);
        return bestPlay;
    }


    /*
     * Hace todos los movimientos posibles y lo pasa al "enemigo" para ver cual es la que maximiza la diferencia de puntos
     * Este no tiene en cuenta ningun tipo de recursion porque va a ser el que tome la "decision" de cual es la mejor jugada posible
     */
/*
    private void maxNode (ArrayList <Card> [] currentTable, ArrayList <Card> currentHand, double alpha, double beta, int depth, int playerId) {

        double bestRes = Double.NEGATIVE_INFINITY;
        Card play = null;

        for (int i = 0; i < currentHand.size (); i++) {

            play = currentHand.get (i);

            currentTable [playerId].add (play);
            currentHand.remove (play);

            alpha = Math.max (minNode (currentTable, currentHand, alpha, beta, depth + 1, (playerId + 1) % 4));

            if (alpha > bestRes) {

                bestRes = alpha;
                bestPlay = play;
            }
            
            if (alpha >= beta) {
                
                break;
            }
            
            currentHand.add (play);
            currentTable [playerId].remove (play);
        }
    }


    /*
     * Hace todos los movimientos posibles para lo mismo que la anterior, pero siendo el "turno del enemigo"
     */
/*
    private double minNode (ArrayList <Card> [] currentTable, ArrayList <Card> currentHand, double alpha, double beta, int depth, int playerId) {

        Card play = null;

        if (depth >= MAX_DEPTH) {

            return evaluate ();
        }

        for (int i = 0; i < currentHand.size (); i++) {

            play = currentHand.get (i);

            currentTable [playerId].add (play);
            currentHand.remove (play);

            beta = Math.min (beta, minNode (currentTable, currentHand, alpha, beta, depth + 1, (playerId + 1) % 4));

            currentHand.add (play);
            currentTable [playerId].remove (play);

            if (alpha >= beta) {

                break;
            }
        }

        return beta;
    }



    /*
     * Aqui se decide cuanto recompensar ciertas jugadas y cuanto castigar otras
     */
/*
    private double evaluate (ArrayList <Card> [] currentTable) {

        return 0;
    }
}*/