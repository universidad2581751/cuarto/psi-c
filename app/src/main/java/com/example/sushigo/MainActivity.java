package com.example.sushigo;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;




public class MainActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //SushiGo juego = new SushiGo();
        // Obtén una referencia al botón
        Button btnShowResume = findViewById(R.id.btnShowImages);
        Button btnStartGame = findViewById(R.id.btnPlayGame);
        // Configura un evento de clic para el botón
        btnShowResume.setOnClickListener(view -> {
            // Al hacer click, inicia la actividad ImagesActivity
            Intent intent = new Intent(MainActivity.this, ResumeActivity.class);
            startActivity(intent);
        });

        btnStartGame.setOnClickListener(view -> {
            // Al hacer click, inicia la actividad InGame
            Intent intent = new Intent(MainActivity.this, InGameActivity.class);
            startActivity(intent);
        });
    }

}