package com.example.sushigo;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;


public class ScoresActivity extends BaseActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scores);
        //vuelve al main, no a la anterior pantalla
        Button btnBackToMain = findViewById(R.id.btnBackToMain);
        btnBackToMain.setOnClickListener(view -> {
            Intent  intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            // Al hacer click, cierra esta actividad y regresa a MainActivity
            finish();
        });
        // Obtén el Intent que envía los resultados
        Intent intent = getIntent();

        // Obtén los resultados (entre comillas el identificador en teoria)
        String resultados = intent.getStringExtra("resultados");

        // Establece el texto del TextView
        TextView textView = findViewById(R.id.textScores);
        textView.setText(resultados);
    }
}
