package com.example.sushigo;

import android.os.Bundle;
import android.widget.Button;

public class ResumeActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.resume);

        Button btnBackToMain = findViewById(R.id.btnBackToMain);
        btnBackToMain.setOnClickListener(view -> {
            // Al hacer click, cierra esta actividad y regresa a MainActivity
            finish();
        });
    }

}
