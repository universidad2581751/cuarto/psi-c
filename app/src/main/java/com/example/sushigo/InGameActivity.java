package com.example.sushigo;

import android.content.ClipData;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.Button;
import android.widget.ImageView;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.color.utilities.Score;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Collections;

import core.*;

public class InGameActivity extends BaseActivity {

    SushiGo sushiGo ;
    private Player currentPlayer;

    ArrayList <Player> players;

    private int initialHandSize;

    private int gameNum = 0;

    private int  round = 0;

    private boolean leastPuddings = false;

    ImageView tutorial;
    TextView tutorialtext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ingame);
        sushiGo = new SushiGo();
        initialHandSize = sushiGo.getInitialHandSize();
        currentPlayer = sushiGo.getHumanPlayer();
        players = sushiGo.getPlayers();
        int tutorialId = getResources().getIdentifier("tutorial", "id", getPackageName());
        tutorial = findViewById(tutorialId);
        tutorial.setVisibility(View.INVISIBLE);
        int tutorialtextId = getResources().getIdentifier("tutorialtext", "id", getPackageName());
        tutorialtext = findViewById(tutorialtextId);
        setupPlayerCards();
        setupBottonImages();
        setupRound();

    }

    public void mostrarPopup(View v){
        ImageView btnBackToMain = findViewById(R.id.btnBackToMain);
        PopupMenu popupMenu = new PopupMenu(InGameActivity.this, btnBackToMain);
        popupMenu.getMenuInflater().inflate(R.menu.menupopup, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if(item.getTitle().equals("Return")) {
                    finish();
                    return true;
                }
                if(item.getTitle().equals("Help")) {

                    tutorial.setImageResource(R.drawable.tutorial3);
                    tutorial.setVisibility(View.VISIBLE);
                    tutorial.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                    tutorial.setElevation(10.0f);

                    tutorialtext.setText("Click on the help image to return");
                    tutorialtext.setVisibility(View.VISIBLE);
                    tutorialtext.setElevation(10.0f);
                    tutorial.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            tutorial.setVisibility(View.INVISIBLE);
                            tutorialtext.setVisibility(View.INVISIBLE);
                        }
                    });
                    return true;
                }
                return false;
            }
        });
        popupMenu.show();
    }

    //Function for displaying the round
    private void setupRound(){
        int RoundId = getResources().getIdentifier("Round", "id", getPackageName());
        TextView Round = findViewById(RoundId);
        Round.setText("Round: "+ (gameNum+1) + "/3");
    }

    //Function for displaying the cards in the player's hand
    private void setupPlayerCards() {
        //Pick the cards
        ArrayList<Card> playerCards = currentPlayer.getCurrentHand();
        int cardIndex = 0;
        //We scroll through the player cards and take the necessary ImageView.
        for (Card card : playerCards) {
            //We have on the screen 8 ImageViews called cardX, where X = 1,2,...,8
            int cardImageViewId = getResources().getIdentifier("card" + (cardIndex + 1), "id", getPackageName());
            ImageView cardImageView = findViewById(cardImageViewId);
            if (cardImageView != null) {
                //If it exists, we add the necessary image and make it clickable.
                int cardImageResourceId = getCardImageResource(card);
                cardImageView.setImageResource(cardImageResourceId);
                cardImageView.setClickable(true);
                cardImageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            }
            cardIndex++;
        }

        // Replace missing cards with transparent image
        for (int i = playerCards.size(); i < initialHandSize; i++) {
            int cardImageViewId = getResources().getIdentifier("card" + (i + 1), "id", getPackageName());
            ImageView cardImageView = findViewById(cardImageViewId);

            if (cardImageView != null) {
                cardImageView.setImageResource(android.R.color.transparent);
                cardImageView.setClickable(false);
                cardImageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            }
        }
    }

    // Function to get the id of the card we pass to it
    private int getCardImageResource(Card card) {
        //We take the type of the card and change it to lowercase to match the images.
        String cardImageName = card.getType().toLowerCase();
        return getResources().getIdentifier(cardImageName, "drawable", getPackageName());
    }

    //We put buttons on the hand cards
    private void setupBottonImages(){
        ArrayList<Card> playerCards = currentPlayer.getCurrentHand();
        int cardIndex = 0;

        for (Card card : playerCards) {
            int cardImageViewId = getResources().getIdentifier("card" + (cardIndex + 1), "id", getPackageName());
            ImageView cardImageView = findViewById(cardImageViewId);
            if (cardImageView != null) {
                // Attach OnClickListener to each ImageView
                int finalCardIndex = cardIndex;
                cardImageView.setOnClickListener(view -> onCardSelected(currentPlayer.getCurrentHand().get(finalCardIndex)));
            }
        cardIndex++;

        }

    }

    //Function that we call every time we click on the cards in the hand.
    private void onCardSelected(Card selectedCard) {

        //If the tutorial is visible we remove it, if not we play the card.
        if(tutorial.getVisibility() == View.INVISIBLE){
            //The first card we cannot use minMax, so we call a function that randomises between the most interesting cards.
            if(round ==0){
                sushiGo.firstPlay(players.get(1));
                sushiGo.firstPlay(players.get(2));
                sushiGo.firstPlay(players.get(3));
            }else{
                sushiGo.iaPlayerPlay(players.get(1),  players.get(0));
                sushiGo.iaPlayerPlay(players.get(2),  players.get(1));
                sushiGo.iaPlayerPlay(players.get(3),  players.get(2));
            }
            sushiGo.humanPlayerPlay(currentPlayer, selectedCard.getType());
            round++;
            sushiGo.rotateHands();
            //update hand
            setupPlayerCards();
            //update table
            setupTable();
            //Show recommendations based on gameplay
            setupRecommendations();
            //At the end of the round
            if(currentPlayer.getCurrentHand().size()==0){
                gameNum += 1;
                //update round
                setupRound();
                round = 0;
                clearTable();
                //If 3 rounds have passed, it ends
                if(gameNum ==3){
                    sushiGo.countPuddings();
                    showFinalScores();

                }
                sushiGo.dealCards();
                //update hand
                setupPlayerCards();
                //update recommendations
                setupRecommendations();
            }
        }else{
            tutorial.setVisibility(View.INVISIBLE);
            tutorial.setElevation(0);
            tutorialtext.setVisibility(View.INVISIBLE);
            tutorialtext.setElevation(0);
        }

    }

    //Function to enlarge the cards
    private class CardTouchListener implements View.OnTouchListener {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            ImageView cardImageView = (ImageView) v;

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    // Raise the elevation so that the chart is above the other charts
                    cardImageView.setElevation(10.0f);
                    // Action when the card is pressed (resize, etc.)
                    cardImageView.setScaleX(4f);
                    cardImageView.setScaleY(4f);
                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:
                    // Restore the elevation to its original value
                    cardImageView.setElevation(0.0f);
                    // Action when card is released or touch event is cancelled (resize)
                    cardImageView.setScaleX(1.0f);
                    cardImageView.setScaleY(1.0f);
                    break;
            }

            // Return true to indicate that the event was handled.
            return true;
        }
    }

    private void setupTable(){

        //We scroll through all players to show the cards played in the different ImageView: CardsPXPlayedY, where X=0-4, Y=1-8.
        for(int playerid=0; playerid<4; playerid++ ){
            ArrayList<Card> tableCardsPlayer = players.get(playerid).getTableHand();
            int cardIndex = 0;

            for (Card card : tableCardsPlayer) {
                int cardImageViewId = getResources().getIdentifier("CardsP"+playerid+"Played" + (cardIndex + 1), "id", getPackageName());
                ImageView cardImageView = findViewById(cardImageViewId);
                if (cardImageView != null) {
                    int cardImageResourceId = getCardImageResource(card);
                    cardImageView.setImageResource(cardImageResourceId);
                    cardImageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                } else {

                }
                cardIndex++;
            }
            //Round end
            if(tableCardsPlayer.size() == 8){
                sushiGo.countPoints();
                showScores();
                break;
            }
        }

    }

    private void clearTable(){
        //Put all the cards on the table in transparent
        for(int playerid=0; playerid<4; playerid++ ){
            ArrayList<Card> tableCardsPlayer = players.get(playerid).getTableHand();
            int cardIndex = 0;

            for (Card card : tableCardsPlayer) {
                int cardImageViewId = getResources().getIdentifier("CardsP"+playerid+"Played" + (cardIndex + 1), "id", getPackageName());
                ImageView cardImageView = findViewById(cardImageViewId);
                if (cardImageView != null) {
                    cardImageView.setImageResource(android.R.color.transparent);
                    cardImageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                }
                cardIndex++;
            }
        }
        for(int Recomen = 1;Recomen < 4 ; Recomen++){
            int cardImageViewId = getResources().getIdentifier("CardReco" + Recomen, "id", getPackageName());
            ImageView cardImageView = findViewById(cardImageViewId);
            if (cardImageView != null) {
                cardImageView.setImageResource(android.R.color.transparent);
                cardImageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            }
        }
    }

    private void showFinalScores(){

        Collections.sort(players, Collections.reverseOrder());
        StringBuilder scoresText = new StringBuilder();
        for (Player player : players) {
            if(player.getId()==0){
                scoresText.append("You: ").append(player.getPoints()).append(" points\n");
            }else{
                scoresText.append("Player ").append(player.getId()).append(": ").append(player.getPoints()).append(" points\n");
            }

        }
        // Create an Intent to send the results
        Intent intent = new Intent(this, ScoresActivity.class);

        // Add the results to the Intent
        intent.putExtra("resultados", scoresText.toString());

        // Call up the results screen (Scores)
        startActivity(intent);
    }

    //Function to display the scores and puddings of each player at the end of the rounds.
    private void showScores(){

        for(int playerid=0; playerid<4; playerid++ ){
            int playerTextViewId = getResources().getIdentifier("textPlayer" + playerid, "id", getPackageName());
            TextView playerTextView = findViewById(playerTextViewId);
            if (playerTextView != null) {
                StringBuilder scoresText;
                if(playerid == 0){
                    scoresText = new StringBuilder("You "+ "\n");
                }else{
                    scoresText = new StringBuilder("Player "+playerid+ "\n");
                }

                scoresText.append(players.get(playerid).getPoints()).append(" points" + "\n").append("Puddings: "+players.get(playerid).getPuddings());
                playerTextView.setText(scoresText.toString());

            }

        }
    }

    //Card recommendation function
    private void setupRecommendations(){
        //We take the cards that we have played
        ArrayList<Card> Cards = currentPlayer.getTableHand();
        int Recommendation =1;
        int Sashimi= 0;
        int Tempura= 0;
        int MakiRoll = 0;
        int Puddindg = 0;
        int Dumpling = 0;
        for (Card card : Cards){
            switch (card.getType()){
                case "Sashimi": Sashimi++;
                break;
                case "Tempura": Tempura++;
                break;
                case "MakiRoll": MakiRoll++;
                break;
                case "Pudding" : Puddindg++;
                break;
                case "Dumpling" : Dumpling++;
                break;
            }

        }
        //We compare the Puddings with those played by others.
        boolean PuddinRec = comparePuddin(Puddindg);
        //We compare the Makis with those played by others.
        boolean MakiRec = compareMaki(MakiRoll);
        //Variables for not repeating recommendations
        boolean firstTempura = true;
        boolean firstSashimi = true;
        boolean firstDumpling = true;
        boolean firstMaki = true;
        boolean firstPudding = true;
        //Pudding is recommended without you playing if you are losing.
        int cardImageViewIdPudding = getResources().getIdentifier("Pudding", "id", getPackageName());
        ImageView PuddingCard = findViewById(cardImageViewIdPudding);

        for (Card card : Cards){
            //As Niguiri is neutral we do not recommend it.
            if(card.getType() != "SalmonNiguiri"){

                int cardImageViewId = getResources().getIdentifier("CardReco" + Recommendation, "id", getPackageName());
                ImageView cardImageView = findViewById(cardImageViewId);

                if (cardImageView != null) {
                    int cardImageResourceId = getCardImageResource(card);

                    cardImageView.setImageResource(android.R.color.transparent);
                    cardImageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                    cardImageView.setOnTouchListener(new CardTouchListener());
                    PuddingCard.setImageResource(android.R.color.transparent);
                    PuddingCard.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                    PuddingCard.setOnTouchListener(new CardTouchListener());

                    //If we don't have a par number of tempuras
                    if(Tempura % 2 != 0 && card.getType().equals("Tempura")){
                        if(firstTempura){
                            cardImageView.setImageResource(cardImageResourceId);
                            cardImageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                            Recommendation++;
                            firstTempura = false;
                        }

                    }
                    //If we don't have a sashimi trio
                    if (Sashimi % 3 != 0 && card.getType().equals("Sashimi")) {
                        if (firstSashimi){
                            cardImageView.setImageResource(cardImageResourceId);
                            cardImageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                            Recommendation++;
                            firstSashimi = false;
                        }

                    }
                    //Dumpling we recommend it until you have the maximum number.
                    if(Dumpling % 5 !=0 && card.getType().equals("Dumpling")){
                        if(firstDumpling){
                            cardImageView.setImageResource(cardImageResourceId);
                            cardImageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                            Recommendation++;
                            firstDumpling = false;
                        }
                    }
                    //Maki is recommended if you are not winning then
                    if(!MakiRec && card.getType().equals("MakiRoll")){
                        if(firstMaki){
                            cardImageView.setImageResource(cardImageResourceId);
                            cardImageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                            Recommendation++;
                            firstMaki = false;
                        }
                    }

                    //Pudding is recommended if you are not winning then
                    if(!PuddinRec && card.getType().equals("Pudding")){
                        if(firstPudding){
                            cardImageView.setImageResource(cardImageResourceId);
                            cardImageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                            Recommendation++;
                            firstPudding = false;
                        }
                    }

                }
            }

        }
        //Pudding is recommended if you are losing them.
        if(leastPuddings && firstPudding){
            PuddingCard.setImageResource(R.drawable.pudding);
            PuddingCard.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

        }

    }

    private boolean compareMaki(int myMakis){
        int mostMakis = 0;
        for(int playerid=1; playerid<4; playerid++ ){
            ArrayList<Card> cardsPlayed = players.get(playerid).getTableHand();
            int Makis = 0;
            for(Card card : cardsPlayed){
                if(card.getType().equals("MakiRoll")){
                    Makis++;
                }
            }
            if(Makis > mostMakis){
                mostMakis = Makis;
            }
        }
        if(myMakis <= mostMakis){
            return false;
        }
        return true;

    }

    private boolean comparePuddin(int myPuddings){
        int mostPudding = 0;
        int leastPudding = 50;
        leastPuddings = false;
        myPuddings += currentPlayer.getPuddings();
        for(int playerid=1; playerid<4; playerid++ ){
            ArrayList<Card> cardsPlayed = players.get(playerid).getTableHand();
            int Puddings = 0;
            for(Card card : cardsPlayed){
                if(card.getType().equals("Pudding")){
                    Puddings++;
                }
            }
            Puddings += players.get(playerid).getPuddings();
            if(Puddings > mostPudding){
                mostPudding = Puddings;
            }
            if(Puddings < leastPudding){
                leastPudding = Puddings;
            }
        }
        if(mostPudding == 0){
            return true;
        }
        if(myPuddings <= leastPudding){
            leastPuddings = true;
            return false;
        }
        if(myPuddings <= mostPudding){
            return false;
        }
        return true;

    }



}
